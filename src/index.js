import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Product from './pages/Product';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Product />
);

