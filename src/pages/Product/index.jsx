import { Box, Grid } from '@mui/material'
import React, { useMemo, useState } from 'react'
import { FilterData, ListProduct } from '../../actions';

export default function Product() {
  const [tabSelected, setTabSelected] = useState('all');
  const listProduct = useMemo(() => {
    if (tabSelected !== 'all')
      return ListProduct?.filter(item => item.categoryName === tabSelected) || [];
    return ListProduct;
  }, [tabSelected]);

  const handleChangeTab = (type) => () => {
    setTabSelected(type);
  };

  return (
    <Box padding={3}>
      <Box
        mt={3}
        mb={7.5}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          padding: '0 16px'
        }}
      >
        {
          FilterData?.map(filter => (
            <Box
              sx={{
                fontSize: '18px',
                padding: '8px 16px',
                borderRadius: '8px',
                marginRight: '10px',
                backgroundColor: filter.type === tabSelected ? '#F8D000' : '',
                fontWeight: '600',
                cursor: 'pointer'
              }}
              key={filter.id}
              onClick={handleChangeTab(filter.type)}
            >
              {
                filter.name
              }
            </Box>
          ))
        }
      </Box>

      <Grid
        container
        spacing={3}
        sx={{
          padding: '0 15px'
        }}>
        {
          listProduct?.map(list => (
            <Grid
              key={list.id}
              item
              xs={4}
              sx={{
                'img': {
                  width: '100%',
                  height: 'auto',
                  borderRadius: '12px',
                },
              }}>
              <Box
                sx={{
                  borderRadius: '12px',
                  position: 'relative',
                  boxShadow: '0px 12px 28px rgb(0 0 0 / 14%)',
                  ':hover': {
                    transform: 'translate(0,-3px)',
                  },
                  transition: 'all .4s cubic-bezier(.165,.84,.44,1)',
                  cursor: 'pointer'
                }}
              >
                <img src={list.imageUrl} alt={list.nameProduct} />
                <Box
                  sx={{
                    fontWeight: '700',
                    padding: '16px',
                    margin: '16px',
                    width: 'calc(100% - 64px)',
                    background: '#FFFFFF',
                    boxShadow: '0px 12px 28px rgb(0 0 0 / 14%)',
                    borderRadius: '8px',
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                  }}
                >
                  {
                    list.nameProduct
                  }
                </Box>
              </Box>
            </Grid>
          ))
        }
      </Grid>
    </Box >
  )
}
