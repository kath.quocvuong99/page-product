export const FilterData = [
  {
    id: 1,
    type: 'all',
    name: 'All',
  },
  {
    id: 2,
    type: 'ms',
    name: 'Manpower Supply',
  },
  {
    id: 3,
    type: 'maw',
    name: 'Mobile App/ Websites',
  },
  {
    id: 4,
    type: 'uiux',
    name: 'UI/ UX Design',
  },
]

export const ListProduct =[
  {
    id: 1,
    nameProduct: 'TOI 3D Customize E-commerce',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/09/image-1.png',
    categoryName: 'ms',
  },
  {
    id: 2,
    nameProduct: 'E-learning – Internal training platform',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/09/image.png',
    categoryName: 'ms',
  },
  {
    id: 3,
    nameProduct: 'Summer 21 Cosmetic E-commerce Platform',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png',
    categoryName: 'maw',
  },
  {
    id: 4,
    nameProduct: 'Kiva – Ambition to digital transformation in the brokerage assiduity',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png',
    categoryName: 'maw',
  },
  {
    id: 5,
    nameProduct: 'Boxgo – Professional Warehouse Management',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/boxgo-thumbnail.png',
    categoryName: 'uiux',
  },{
    id: 6,
    nameProduct: 'TOI 3D Customize E-commerce',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/09/image-1.png',
    categoryName: 'ms',
  },
  {
    id: 7,
    nameProduct: 'E-learning – Internal training platform',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/09/image.png',
    categoryName: 'ms',
  },
  {
    id: 8,
    nameProduct: 'Summer 21 Cosmetic E-commerce Platform',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png',
    categoryName: 'maw',
  },
  {
    id: 9,
    nameProduct: 'Kiva – Ambition to digital transformation in the brokerage assiduity',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png',
    categoryName: 'uiux',
  },
  {
    id: 10,
    nameProduct: 'Boxgo – Professional Warehouse Management',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/boxgo-thumbnail.png',
    categoryName: 'uiux',
  },
  {
    id: 11,
    nameProduct: 'Kiva – Ambition to digital transformation in the brokerage assiduity',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png',
    categoryName: 'uiux',
  },
  {
    id: 12,
    nameProduct: 'Boxgo – Professional Warehouse Management',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/boxgo-thumbnail.png',
    categoryName: 'uiux',
  },
  {
    id: 13,
    nameProduct: 'Summer 21 Cosmetic E-commerce Platform',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png',
    categoryName: 'maw',
  },
  {
    id: 14,
    nameProduct: 'Summer 21 Cosmetic E-commerce Platform',
    imageUrl: 'https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png',
    categoryName: 'maw',
  },
]